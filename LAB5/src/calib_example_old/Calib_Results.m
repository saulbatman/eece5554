% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 3386.031538994880975 ; 3398.109751392304133 ];

%-- Principal point:
cc = [ 1992.955889935266441 ; 1537.955711386301800 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.367820436350592 ; -1.409671630454133 ; 0.006133416408506 ; 0.002426193763105 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 28.414213000734094 ; 33.497424851013996 ];

%-- Principal point uncertainty:
cc_error = [ 29.592095348928034 ; 31.514711692658061 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.048267582385701 ; 0.295232978669839 ; 0.005203916424375 ; 0.004486041004403 ; 0.000000000000000 ];

%-- Image size:
nx = 4032;
ny = 3024;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 26;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 2.525675e+00 ; 3.332833e-02 ; 1.526068e-01 ];
Tc_1  = [ -8.662080e+01 ; 5.061850e+01 ; 4.733619e+02 ];
omc_error_1 = [ 1.305030e-02 ; 4.924567e-03 ; 1.572998e-02 ];
Tc_error_1  = [ 4.140191e+00 ; 4.440603e+00 ; 4.555834e+00 ];

%-- Image #2:
omc_2 = [ 1.825747e+00 ; 1.629050e+00 ; -3.321665e-02 ];
Tc_2  = [ -7.325163e+01 ; -5.671596e+01 ; 3.818222e+02 ];
omc_error_2 = [ 8.362711e-03 ; 8.097048e-03 ; 1.308926e-02 ];
Tc_error_2  = [ 3.349574e+00 ; 3.533508e+00 ; 3.502007e+00 ];

%-- Image #3:
omc_3 = [ 1.872622e+00 ; 1.987392e+00 ; 4.309151e-01 ];
Tc_3  = [ -3.201695e+01 ; -6.617695e+01 ; 4.083809e+02 ];
omc_error_3 = [ 9.769914e-03 ; 8.630544e-03 ; 1.585472e-02 ];
Tc_error_3  = [ 3.602995e+00 ; 3.778238e+00 ; 4.509107e+00 ];

%-- Image #4:
omc_4 = [ -2.059585e+00 ; -2.110106e+00 ; 5.602344e-01 ];
Tc_4  = [ -1.017489e+02 ; -6.939760e+01 ; 4.395970e+02 ];
omc_error_4 = [ 1.069354e-02 ; 8.413465e-03 ; 1.774095e-02 ];
Tc_error_4  = [ 3.850591e+00 ; 4.105311e+00 ; 3.552507e+00 ];

%-- Image #5:
omc_5 = [ -2.015008e+00 ; -2.273563e+00 ; 2.797997e-01 ];
Tc_5  = [ -1.799278e+02 ; -1.085160e+02 ; 6.198312e+02 ];
omc_error_5 = [ 1.505716e-02 ; 1.331314e-02 ; 2.917895e-02 ];
Tc_error_5  = [ 5.631722e+00 ; 5.899857e+00 ; 5.810850e+00 ];

%-- Image #6:
omc_6 = [ 1.611803e+00 ; 1.821727e+00 ; -1.287180e-01 ];
Tc_6  = [ -2.494440e+01 ; -6.385344e+01 ; 3.352118e+02 ];
omc_error_6 = [ 8.099640e-03 ; 8.052683e-03 ; 1.285435e-02 ];
Tc_error_6  = [ 2.917251e+00 ; 3.074979e+00 ; 2.836074e+00 ];

%-- Image #7:
omc_7 = [ 1.640855e+00 ; 1.985871e+00 ; -5.993127e-01 ];
Tc_7  = [ -5.600172e+01 ; -6.722360e+01 ; 3.173308e+02 ];
omc_error_7 = [ 6.400288e-03 ; 9.263529e-03 ; 1.265583e-02 ];
Tc_error_7  = [ 2.765861e+00 ; 2.920776e+00 ; 2.235020e+00 ];

%-- Image #8:
omc_8 = [ 1.954852e+00 ; 2.045286e+00 ; 9.513656e-01 ];
Tc_8  = [ -3.466369e+01 ; -6.964988e+01 ; 2.760242e+02 ];
omc_error_8 = [ 9.742190e-03 ; 6.792453e-03 ; 1.435750e-02 ];
Tc_error_8  = [ 2.470710e+00 ; 2.558339e+00 ; 3.001473e+00 ];

%-- Image #9:
omc_9 = [ 2.158032e+00 ; 2.282629e+00 ; 1.934004e-01 ];
Tc_9  = [ -1.115430e+02 ; -8.621084e+01 ; 7.830852e+02 ];
omc_error_9 = [ 4.119531e-02 ; 4.705809e-02 ; 9.766882e-02 ];
Tc_error_9  = [ 6.961111e+00 ; 7.342208e+00 ; 1.041940e+01 ];

%-- Image #10:
omc_10 = [ -1.849266e+00 ; -2.059471e+00 ; -6.206599e-01 ];
Tc_10  = [ -1.357996e+02 ; -1.506849e+01 ; 8.088679e+02 ];
omc_error_10 = [ 1.278836e-02 ; 1.488357e-02 ; 2.434669e-02 ];
Tc_error_10  = [ 7.120491e+00 ; 7.568449e+00 ; 8.473584e+00 ];

%-- Image #11:
omc_11 = [ NaN ; NaN ; NaN ];
Tc_11  = [ NaN ; NaN ; NaN ];
omc_error_11 = [ NaN ; NaN ; NaN ];
Tc_error_11  = [ NaN ; NaN ; NaN ];

%-- Image #12:
omc_12 = [ -1.675079e+00 ; -2.195164e+00 ; -9.192296e-01 ];
Tc_12  = [ -6.257272e+01 ; -4.182690e+01 ; 5.428152e+02 ];
omc_error_12 = [ 8.710439e-03 ; 1.250672e-02 ; 1.711341e-02 ];
Tc_error_12  = [ 4.773933e+00 ; 5.064118e+00 ; 5.550636e+00 ];

%-- Image #13:
omc_13 = [ 2.008033e+00 ; 2.318224e+00 ; -4.673826e-01 ];
Tc_13  = [ -9.088427e+01 ; -6.641855e+01 ; 4.296697e+02 ];
omc_error_13 = [ 9.497040e-03 ; 1.274569e-02 ; 2.012918e-02 ];
Tc_error_13  = [ 3.731603e+00 ; 3.997632e+00 ; 3.494450e+00 ];

%-- Image #14:
omc_14 = [ -1.671124e+00 ; -1.796948e+00 ; -8.705312e-01 ];
Tc_14  = [ -1.182874e+02 ; -2.868191e+01 ; 6.500464e+02 ];
omc_error_14 = [ 9.527616e-03 ; 1.157556e-02 ; 1.532378e-02 ];
Tc_error_14  = [ 5.710956e+00 ; 6.080169e+00 ; 6.517082e+00 ];

%-- Image #15:
omc_15 = [ NaN ; NaN ; NaN ];
Tc_15  = [ NaN ; NaN ; NaN ];
omc_error_15 = [ NaN ; NaN ; NaN ];
Tc_error_15  = [ NaN ; NaN ; NaN ];

%-- Image #16:
omc_16 = [ NaN ; NaN ; NaN ];
Tc_16  = [ NaN ; NaN ; NaN ];
omc_error_16 = [ NaN ; NaN ; NaN ];
Tc_error_16  = [ NaN ; NaN ; NaN ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ NaN ; NaN ; NaN ];
Tc_error_17  = [ NaN ; NaN ; NaN ];

%-- Image #18:
omc_18 = [ 1.870659e+00 ; 1.951260e+00 ; -5.306749e-01 ];
Tc_18  = [ -1.100602e+02 ; -4.635542e+01 ; 4.679909e+02 ];
omc_error_18 = [ 8.253465e-03 ; 1.111632e-02 ; 1.626811e-02 ];
Tc_error_18  = [ 4.068790e+00 ; 4.378457e+00 ; 3.703205e+00 ];

%-- Image #19:
omc_19 = [ NaN ; NaN ; NaN ];
Tc_19  = [ NaN ; NaN ; NaN ];
omc_error_19 = [ NaN ; NaN ; NaN ];
Tc_error_19  = [ NaN ; NaN ; NaN ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

%-- Image #21:
omc_21 = [ -1.848305e+00 ; -2.049458e+00 ; -5.877703e-03 ];
Tc_21  = [ -1.787664e+02 ; -7.883277e+01 ; 5.054223e+02 ];
omc_error_21 = [ 1.077545e-02 ; 9.918732e-03 ; 2.298871e-02 ];
Tc_error_21  = [ 4.537671e+00 ; 4.817724e+00 ; 4.921044e+00 ];

%-- Image #22:
omc_22 = [ -1.733646e+00 ; -1.381600e+00 ; -3.241026e-01 ];
Tc_22  = [ -1.683852e+02 ; -2.576732e+00 ; 4.832280e+02 ];
omc_error_22 = [ 9.152193e-03 ; 8.052822e-03 ; 1.163296e-02 ];
Tc_error_22  = [ 4.219715e+00 ; 4.613343e+00 ; 4.348221e+00 ];

%-- Image #23:
omc_23 = [ -1.561728e+00 ; -2.023143e+00 ; -1.047824e+00 ];
Tc_23  = [ -5.712248e+01 ; -3.133497e+01 ; 1.907758e+02 ];
omc_error_23 = [ 6.245968e-03 ; 1.030350e-02 ; 1.224072e-02 ];
Tc_error_23  = [ 1.672688e+00 ; 1.810918e+00 ; 1.959664e+00 ];

%-- Image #24:
omc_24 = [ -1.573776e+00 ; -2.469938e+00 ; -7.896260e-01 ];
Tc_24  = [ -3.643493e+01 ; -6.529469e+01 ; 2.627333e+02 ];
omc_error_24 = [ 5.702192e-03 ; 1.121116e-02 ; 1.515748e-02 ];
Tc_error_24  = [ 2.322956e+00 ; 2.469979e+00 ; 2.507688e+00 ];

%-- Image #25:
omc_25 = [ -1.591363e+00 ; -2.227402e+00 ; -9.773152e-01 ];
Tc_25  = [ -1.765399e+01 ; -2.932131e+01 ; 3.205020e+02 ];
omc_error_25 = [ 6.043301e-03 ; 1.135404e-02 ; 1.401330e-02 ];
Tc_error_25  = [ 2.801398e+00 ; 2.979038e+00 ; 3.072386e+00 ];

%-- Image #26:
omc_26 = [ 1.550679e+00 ; 2.146470e+00 ; 1.197315e+00 ];
Tc_26  = [ -3.296034e+01 ; -5.014115e+01 ; 2.719256e+02 ];
omc_error_26 = [ 9.459603e-03 ; 6.790622e-03 ; 1.349447e-02 ];
Tc_error_26  = [ 2.409244e+00 ; 2.541709e+00 ; 2.922043e+00 ];

