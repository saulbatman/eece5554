(cl:in-package mygps-msg)
(cl:export '(HEADER-VAL
          HEADER
          CHILD_FRAME_ID-VAL
          CHILD_FRAME_ID
          LATITUDE-VAL
          LATITUDE
          LONGITUDE-VAL
          LONGITUDE
          ALTITUDE-VAL
          ALTITUDE
          UTM_EASTING-VAL
          UTM_EASTING
          UTM_NORTHING-VAL
          UTM_NORTHING
          QUALITY-VAL
          QUALITY
          ZONE-VAL
          ZONE
          LETTER-VAL
          LETTER
))