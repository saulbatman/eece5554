# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "myimu: 1 messages, 0 services")

set(MSG_I_FLAGS "-Imyimu:/home/mingxi/eece5554/LAB4/src/myimu/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/noetic/share/geometry_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/noetic/share/sensor_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(myimu_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg" NAME_WE)
add_custom_target(_myimu_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "myimu" "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg" "sensor_msgs/MagneticField:sensor_msgs/Imu:geometry_msgs/Quaternion:std_msgs/Header:geometry_msgs/Vector3"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(myimu
  "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/MagneticField.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/Imu.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/myimu
)

### Generating Services

### Generating Module File
_generate_module_cpp(myimu
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/myimu
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(myimu_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(myimu_generate_messages myimu_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg" NAME_WE)
add_dependencies(myimu_generate_messages_cpp _myimu_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(myimu_gencpp)
add_dependencies(myimu_gencpp myimu_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS myimu_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(myimu
  "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/MagneticField.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/Imu.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/myimu
)

### Generating Services

### Generating Module File
_generate_module_eus(myimu
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/myimu
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(myimu_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(myimu_generate_messages myimu_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg" NAME_WE)
add_dependencies(myimu_generate_messages_eus _myimu_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(myimu_geneus)
add_dependencies(myimu_geneus myimu_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS myimu_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(myimu
  "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/MagneticField.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/Imu.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/myimu
)

### Generating Services

### Generating Module File
_generate_module_lisp(myimu
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/myimu
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(myimu_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(myimu_generate_messages myimu_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg" NAME_WE)
add_dependencies(myimu_generate_messages_lisp _myimu_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(myimu_genlisp)
add_dependencies(myimu_genlisp myimu_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS myimu_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(myimu
  "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/MagneticField.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/Imu.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/myimu
)

### Generating Services

### Generating Module File
_generate_module_nodejs(myimu
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/myimu
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(myimu_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(myimu_generate_messages myimu_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg" NAME_WE)
add_dependencies(myimu_generate_messages_nodejs _myimu_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(myimu_gennodejs)
add_dependencies(myimu_gennodejs myimu_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS myimu_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(myimu
  "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/MagneticField.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/Imu.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/myimu
)

### Generating Services

### Generating Module File
_generate_module_py(myimu
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/myimu
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(myimu_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(myimu_generate_messages myimu_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/myimu/msg/imu.msg" NAME_WE)
add_dependencies(myimu_generate_messages_py _myimu_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(myimu_genpy)
add_dependencies(myimu_genpy myimu_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS myimu_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/myimu)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/myimu
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(myimu_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(myimu_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(myimu_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/myimu)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/myimu
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(myimu_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(myimu_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(myimu_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/myimu)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/myimu
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(myimu_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(myimu_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(myimu_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/myimu)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/myimu
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(myimu_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(myimu_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(myimu_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/myimu)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/myimu\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/myimu
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(myimu_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(myimu_generate_messages_py geometry_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(myimu_generate_messages_py sensor_msgs_generate_messages_py)
endif()
