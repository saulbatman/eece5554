# !/usr/bin/env python

from re import L
import rospy
from std_msgs.msg import String   #message type
from myimu.msg import imu
import serial
from scipy.spatial.transform import Rotation as R
from sensor_msgs.msg import MagneticField, Imu


def serialtalker():
    IMU_pub = rospy.Publisher('imu_data', imu, queue_size=10)
    # gps_talker is the node name
    rospy.init_node('IMU_talker', anonymous=True)
    IMU_info = imu()
    IMU_info.header.frame_id = "IMU"
    IMU_info.child_frame_id = "VN100"
    IMU_info.header.seq=0

    rate = rospy.Rate(40) # rate is 1 because gps rate is around 1
    serial_port = rospy.get_param('~port','/dev/ttyUSB0')
    # serial_port = rospy.get_param('~port','/dev/pts/6')
    serial_baud = rospy.get_param('~baudrate',115200)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    while not rospy.is_shutdown():
        line  = port.readline()
        line=line.decode('utf-8')
        # rospy.loginfo(line)
        if line != '':
            a = line.split(",")
            if 'VNYMR' in a[0]:
                print(line)
                if a[2] != "":
                    try:
                        # Yaw, Pitch, Roll, Magnetic, Acceleration, and Angular Rate Measurements


                        IMU_info.ypr.x = float(a[1])
                        IMU_info.ypr.y = float(a[2])
                        IMU_info.ypr.z =  float(a[3])

                        IMU_info.magnetic.magnetic_field.x = float(a[4])
                        IMU_info.magnetic.magnetic_field.y = float(a[5])
                        IMU_info.magnetic.magnetic_field.z = float(a[6])
                        
                        IMU_info.imu.linear_acceleration.x = float(a[7])
                        IMU_info.imu.linear_acceleration.y = float(a[8])
                        IMU_info.imu.linear_acceleration.z = float(a[9])

                        last = (a[12]).split("*")
                        IMU_info.imu.angular_velocity.x = float(a[10])
                        IMU_info.imu.angular_velocity.y = float(a[11])
                        IMU_info.imu.angular_velocity.z = float(last[0])
                        

                        r = R.from_euler('zyx', [IMU_info.ypr.z, IMU_info.ypr.y, IMU_info.ypr.x], degrees=True)
                        quaternion=r.as_quat()
                        IMU_info.imu.orientation.x = quaternion[0]
                        IMU_info.imu.orientation.y = quaternion[1]
                        IMU_info.imu.orientation.z = quaternion[2]
                        IMU_info.imu.orientation.w = quaternion[3]
                        IMU_info.imu.header.stamp=rospy.Time.now()   
                        IMU_info.magnetic.header.stamp=rospy.Time.now()   
                        IMU_info.header.stamp=rospy.Time.now()   
                    except:
                        rate.sleep()
                        continue
        rospy.loginfo(IMU_info)
        IMU_pub.publish(IMU_info)

        rate.sleep()
if __name__ == '__main__':
    try:
        # talker()
        serialtalker()
    except rospy.ROSInitException:
        pass