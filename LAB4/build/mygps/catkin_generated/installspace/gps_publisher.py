# !/usr/bin/env python

from re import L
import rospy
from std_msgs.msg import String   #message type
from eece5554.msg import gps
import utm
import serial

def degreetodecimal(degree):
    degree = str(degree)
    temp = degree.split('.')
    if len(temp[0]) == 4:
        # Latitude (DDmm.mm)
        DD_decimal = float(degree[:2])
        mm_decimal = float(degree[2:])/60
    elif len(temp[0]) == 5:
        # Latitude (DDDmm.mm)
        DD_decimal = float(degree[:3])
        mm_decimal = float(degree[3:])/60
    else:
        print('wrong gps degree input')
    return DD_decimal + mm_decimal

# def txt_talker():
#     gps_pub = rospy.Publisher('gps_data', gps, queue_size=10)
#     gps_info = gps()
#     gps_info.header.frame_id = "gps"
#     gps_info.child_frame_id = "BU-353"
#     gps_info.header.seq=0
#     # gps_talker is the node name
#     rospy.init_node('gps_talker', anonymous=True)
#     rate = rospy.Rate(1) # rate is 1 because gps rate is around 1
#     file_length = 0 # check whther the log is continouly updating
#     while not rospy.is_shutdown():
#         lines = []
#         with open('/home/mingxi/eece_data/gps-data.txt') as f:
#             lines = f.readlines()
#         # count = 0
#         if (len(lines) == file_length) and (file_length !=0):
#             rospy.loginfo("you are using old data")
#         for line in lines[-8:-1]:    # gps everytime receives a group of 8 lines
#             # count += 1
#             a=line.split(",")
#             if a[0] == "$GPGGA":
#                 if a[2] is not None:
                    
#                     if a[3] == "N":
#                         gps_info.latitude = float(degreetodecimal(a[2]))
#                     else:
#                         gps_info.latitude = -float(degreetodecimal(a[2]))
#                     if a[5] == "E":
#                         gps_info.longitude = float(degreetodecimal(a[4]))
#                     else:
#                         gps_info.longitude = -float(degreetodecimal(a[4]))

#                     utm_info = utm.from_latlon(gps_info.latitude, gps_info.longitude) # from latlon to utm
#                     gps_info.altitude = float(a[9])
#                     gps_info.utm_easting=utm_info[0]
#                     gps_info.utm_northing=utm_info[1]
#                     gps_info.zone=utm_info[2]
#                     gps_info.letter=utm_info[3]
#                 else:
#                     print("receiving data, but no available gps position")
#             # else:
#             #     print("not receiving data")

#         file_length = len(lines)
#         rospy.loginfo(gps_info)
#         gps_pub.publish(gps_info)
#         rate.sleep()


def serialtalker():
    gps_pub = rospy.Publisher('gps_data', gps, queue_size=10)
    gps_info = gps()
    gps_info.header.frame_id = "gps"
    gps_info.child_frame_id = "RTK"
    gps_info.header.seq=0
    # gps_talker is the node name
    rospy.init_node('gps_talker', anonymous=True)
    rate = rospy.Rate(8) # rate is 1 because gps rate is around 1
    serial_port = rospy.get_param('~port','/dev/ttyACM0')
    serial_baud = rospy.get_param('~baudrate',57600)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    while not rospy.is_shutdown():
        line  = str(port.readline())
        # rospy.loginfo(line)
        if line != '':
            a = line.split(",")
            if a[0] == 'b\'$GPGGA':
                print(line)
                if a[2] != "":
                    if a[3] == "N":
                        gps_info.latitude = float(degreetodecimal(a[2]))
                    else:
                        gps_info.latitude = -float(degreetodecimal(a[2]))
                    if a[5] == "E":
                        gps_info.longitude = float(degreetodecimal(a[4]))
                    else:
                        gps_info.longitude = -float(degreetodecimal(a[4]))
                    gps_info.quality = int(a[6])
                    utm_info = utm.from_latlon(gps_info.latitude, gps_info.longitude) # from latlon to utm
                    gps_info.altitude = float(a[9])
                    gps_info.utm_easting = utm_info[0]
                    gps_info.utm_northing = utm_info[1]
                    gps_info.zone = utm_info[2]
                    gps_info.letter = utm_info[3]
                    gps_info.header.stamp=rospy.Time.now()     
        rospy.loginfo(gps_info)
        gps_pub.publish(gps_info)
        rate.sleep()
if __name__ == '__main__':
    try:
        # talker()
        serialtalker()
    except rospy.ROSInitException:
        pass