# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "mygps: 1 messages, 0 services")

set(MSG_I_FLAGS "-Imygps:/home/mingxi/eece5554/LAB4/src/mygps/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(mygps_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg" NAME_WE)
add_custom_target(_mygps_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "mygps" "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg" "std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(mygps
  "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mygps
)

### Generating Services

### Generating Module File
_generate_module_cpp(mygps
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mygps
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(mygps_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(mygps_generate_messages mygps_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg" NAME_WE)
add_dependencies(mygps_generate_messages_cpp _mygps_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mygps_gencpp)
add_dependencies(mygps_gencpp mygps_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mygps_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(mygps
  "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mygps
)

### Generating Services

### Generating Module File
_generate_module_eus(mygps
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mygps
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(mygps_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(mygps_generate_messages mygps_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg" NAME_WE)
add_dependencies(mygps_generate_messages_eus _mygps_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mygps_geneus)
add_dependencies(mygps_geneus mygps_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mygps_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(mygps
  "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mygps
)

### Generating Services

### Generating Module File
_generate_module_lisp(mygps
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mygps
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(mygps_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(mygps_generate_messages mygps_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg" NAME_WE)
add_dependencies(mygps_generate_messages_lisp _mygps_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mygps_genlisp)
add_dependencies(mygps_genlisp mygps_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mygps_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(mygps
  "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mygps
)

### Generating Services

### Generating Module File
_generate_module_nodejs(mygps
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mygps
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(mygps_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(mygps_generate_messages mygps_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg" NAME_WE)
add_dependencies(mygps_generate_messages_nodejs _mygps_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mygps_gennodejs)
add_dependencies(mygps_gennodejs mygps_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mygps_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(mygps
  "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mygps
)

### Generating Services

### Generating Module File
_generate_module_py(mygps
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mygps
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(mygps_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(mygps_generate_messages mygps_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB4/src/mygps/msg/gps.msg" NAME_WE)
add_dependencies(mygps_generate_messages_py _mygps_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mygps_genpy)
add_dependencies(mygps_genpy mygps_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mygps_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mygps)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mygps
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(mygps_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mygps)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mygps
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(mygps_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mygps)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mygps
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(mygps_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mygps)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mygps
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(mygps_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mygps)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mygps\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mygps
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(mygps_generate_messages_py std_msgs_generate_messages_py)
endif()
