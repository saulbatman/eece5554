%%%%% Estimate the forward velocity
%%%%%% Magnetometer Calibration
% 
% bag = rosbag("./LAB4data/moving.bag");
% bSel_imu = select(bag,'Topic','/imu_data');
% msgStructs_imu = readMessages(bSel_imu,'DataFormat','struct');
% time_imu= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs_imu);
% bSel_gps = select(bag,'Topic','/gps_data');
% msgStructs_gps = readMessages(bSel_gps,'DataFormat','struct');
time_imu_Sec= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs_imu);
time_imu_Nsec= cellfun(@(m) double(m.Header.Stamp.Nsec), msgStructs_imu);
time_imu=time_imu_Sec+time_imu_Nsec/1e9;
imu_ax= cellfun(@(m) double(m.Imu.LinearAcceleration.X), msgStructs_imu);
imu_ay= cellfun(@(m) double(m.Imu.LinearAcceleration.Y), msgStructs_imu);
imu_az= cellfun(@(m) double(m.Imu.LinearAcceleration.Z), msgStructs_imu);
imu_vx = cumtrapz(time_imu,imu_ax-0.18);
imu_vy = cumtrapz(time_imu,imu_ay+0.22);
figure
plot(imu_vx)
hold on
plot(imu_vy)
legend('imu_vx','imu_vy')
hold off;