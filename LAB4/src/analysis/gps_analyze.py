from cProfile import label
import numpy as np
import matplotlib.pylab as plt
import mplleaflet
from sklearn import linear_model
import utm
# count = 0



def plot_utm(lines, data_path, with_plot=True, with_plot_hist=False):
    utm_easting_list = []
    utm_northing_list = []
    altitude_list = []
    for line in lines: 
        # count += 1
        line = line.strip()
        a=line.split(": ")
        if a[0] == "utm_easting":
            utm_easting_list.append(a[1])
        if a[0] == "utm_northing":
            utm_northing_list.append(a[1])
        if a[0] == "altitude":
            altitude_list.append(a[1])

    if len(utm_easting_list) == len(utm_northing_list):

        utm_easting_list = np.array(utm_easting_list)
        utm_northing_list = np.array(utm_northing_list)
        
        utm_data=np.zeros([len(utm_northing_list), 2])
        utm_data[:,0] = utm_easting_list
        utm_data[:,1] = utm_northing_list


        average_x, average_y = utm_data.mean(0)
        range_x = utm_data[:,0].max()-utm_data[:,0].min()
        range_y = utm_data[:,1].max()-utm_data[:,1].min()



        # centerized plot
        plt.figure()
        plt.scatter(utm_data[:,0]-average_x, utm_data[:,1]-average_y, marker='.', label="data points")
        plt.scatter(0, 0, marker='*', label="average point")

        plt.grid()
        title = data_path[7:-5]+"_static_utm_plot_centered"
        plt.title(title)
        plt.legend()
        if with_plot==True:
            plt.savefig('./imgs/%s.png'%title)



        # directly plot
        plt.figure()
        plt.scatter(utm_data[:,0], utm_data[:,1], marker=".", label="data points")
        plt.scatter(average_x, average_y, marker="*", label="average point")

        plt.grid()
        title = data_path[7:-5]+"_utm_plot_orin"
        plt.title(title)
        plt.legend()
        if with_plot==True:
            plt.savefig('./imgs/%s.png'%title)



        # plot altitude
        plt.figure()
        altitude_list = np.array(altitude_list).astype(float)
        plt.plot(altitude_list, "go", label="data points")
        plt.hist(altitude_list-altitude_list.mean())

        # plt.yticks(altitude_list[::600])
        plt.grid()
        
        plt.title(title)
        plt.legend()
        if with_plot==True:
            title = data_path[7:-5]+"_altitude_plot"
            plt.savefig('./imgs/%s.png'%title)

        plt.figure()
        plt.hist(altitude_list-altitude_list.mean())
        if with_plot_hist==True:
            title = data_path[7:-5]+"_altitude_hist"
            plt.savefig('./imgs/%s.png'%title)



        # data analysis
        variance_2d = np.std(utm_data)

        data_report = {"range_easting: ": range_x,
                       "range_northing: ": range_y,
                       "2d variance: ": variance_2d,
                       "average utm coordinate: ": (average_x, average_y),}

        print(data_report)
        # plt.show()
        

    else:
        print("data is incorrect")
        
def analyze_static(lines):
    utm_easting_list = []
    utm_northing_list = []
    altitude_list = []
    for line in lines: 
        # count += 1
        line = line.strip()
        a=line.split(": ")
        if a[0] == "utm_easting":
            utm_easting_list.append(a[1])
        if a[0] == "utm_northing":
            utm_northing_list.append(a[1])
        if a[0] == "altitude":
            altitude_list.append(a[1])

    if len(utm_easting_list) == len(utm_northing_list):
        utm_easting_list = np.array(utm_easting_list).astype(float)
        utm_northing_list = np.array(utm_northing_list).astype(float)
        altitude_list = np.array(altitude_list).astype(float)

        utm_data=np.zeros([len(utm_northing_list), 2])
        utm_data[:,0] = utm_easting_list
        utm_data[:,1] = utm_northing_list


        average_x, average_y = utm_data.mean(0)
        avg_data = np.array([average_x, average_y])
        # distances = np.linalg.norm(utm_data-avg_data, axis=1)
        # distances = (utm_data-avg_data)
        centered_data = (utm_data-avg_data).astype(float)
        distance_list=np.array([])
        for i in range(len(centered_data)):
            distances = np.linalg.norm([centered_data[i]], ord=1,axis=1)
            
            if centered_data[i,0]<0:
                distance_list = np.append(distance_list, -distances)
            else:
                distance_list = np.append(distance_list, distances)
        print('easting_range:', utm_easting_list.max()-utm_easting_list.min())
        print('northing_range:', utm_northing_list.max()-utm_northing_list.min())   
        print("std variance: ", np.std(centered_data))    
        plt.hist(altitude_list-altitude_list.mean())


        plt.show()





def analyze_line(lines, data_path, gt, with_pic=True, use_RANSAC=False):
    utm_easting_list = []
    utm_northing_list = []
    altitude_list = []
    for line in lines: 
        # count += 1
        line = line.strip()
        a=line.split(": ")
        if a[0] == "utm_easting":
            utm_easting_list.append(a[1])
        if a[0] == "utm_northing":
            utm_northing_list.append(a[1])
        if a[0] == "altitude":
            altitude_list.append(a[1])


    if len(utm_easting_list) == len(utm_northing_list):
        moving_start_latlon, moving_end_latlon = gt
        start_info= utm.from_latlon(moving_start_latlon[0], moving_start_latlon[1])
        end_info=utm.from_latlon(moving_end_latlon[0], moving_end_latlon[1])
        moving_utm = np.array([start_info[:2],end_info[:2]])
        temp = moving_utm[0]-moving_utm[1]
        slope = temp[1]/temp[0]
        bias = (start_info[1]-slope*start_info[0])
        # print(moving_utm)
        # print(utm_easting_list[0], utm_northing_list[0])

        utm_easting_list = np.array(utm_easting_list).reshape(-1,1).astype(float)
        utm_northing_list = np.array(utm_northing_list).reshape(-1,1).astype(float)

        ####**************Ref:Sci-learn Doc****************####
        ####https://scikit-learn.org/stable/auto_examples/linear_model/plot_ransac.html
        # Fit line using all data
        lr = linear_model.LinearRegression()
        lr.fit(utm_easting_list, utm_northing_list)
        # Predict data of estimated models
        line_X = np.arange(utm_easting_list.min(), utm_easting_list.max())[:, np.newaxis]
        line_y = lr.predict(line_X)

        if use_RANSAC==True:
            # Robustly fit linear model with RANSAC algorithm
            residual_threshold = np.median(np.abs(utm_northing_list - np.median(utm_northing_list)))*0.5
            ransac = linear_model.RANSACRegressor(residual_threshold=residual_threshold)
            ransac.fit(utm_easting_list, utm_northing_list)
            inlier_mask = ransac.inlier_mask_
            outlier_mask = np.logical_not(inlier_mask)
            line_y_ransac = ransac.predict(line_X)
            plt.scatter(
                utm_easting_list[inlier_mask], utm_northing_list[inlier_mask], color="yellowgreen", label="Inliers"
            )
            plt.scatter(
                utm_easting_list[outlier_mask], utm_northing_list[outlier_mask], color="gold",  label="Outliers"
            )
        lw = 2
        
        if use_RANSAC ==False:
            plt.scatter(
                utm_easting_list, utm_northing_list, color="gold", marker=".", label="position"
            )

        plt.plot(line_X, line_y, color="navy", linewidth=lw, label="Linear regressor")
        plt.plot(moving_utm[:, 0], moving_utm[:,1], color='g', label='ground_truth', linewidth=lw)
        plt.plot(
            line_X,
            line_y_ransac,
            color="cornflowerblue",
            linewidth=lw,
            label="RANSAC regressor",
        )
        ####**********************************************###
        # print()
        slope_lr = (lr.predict([utm_easting_list[0]])-lr.predict([utm_easting_list[10]]))/(utm_easting_list[0]-utm_easting_list[10])
        print(slope_lr)
        bias_lr = lr.predict([utm_easting_list[0]])-slope_lr*utm_easting_list[0]
        # Calculation MSE Error
        predicted_northing = lr.predict(utm_easting_list)
        L1Error = np.mean(abs(predicted_northing - utm_northing_list))
        L2Error = np.mean((predicted_northing - utm_northing_list)**2)
        slopeError = abs(slope_lr-slope)
        biasError = abs(bias-bias_lr)
        print('easting_range:', utm_easting_list.max()-utm_easting_list.min())
        print('northing_range:', utm_northing_list.max()-utm_northing_list.min())
        print("L1Error: ", L1Error)
        print("L2Error: ", L2Error)
        print("slopeError: ", slopeError)
        print("biasError: ", biasError)

        if use_RANSAC==True:
            slope_ransac = (ransac.predict([utm_easting_list[0]])-ransac.predict([utm_easting_list[10]]))/(utm_easting_list[0]-utm_easting_list[10])
            print(slope_ransac)
            bias_ransac = ransac.predict([utm_easting_list[0]])-slope_ransac*utm_easting_list[0]
            slopeError = abs(slope_ransac-slope)
            biasError = abs(bias-bias_ransac)
            print("slopeError for RANSAC: ", slopeError)
            print("biasError for RANSAC: ", biasError)


        # plot
        plt.legend()
        title = data_path[7:-5]+"_fit_line"
        plt.title(title)
        if with_pic == True:
            plt.savefig('./imgs/%s.png'%title)

        plt.figure()
        histerror = predicted_northing[np.invert(outlier_mask)] - utm_northing_list[np.invert(outlier_mask)]
        plt.hist(histerror)
        title=data_path[7:-5]+'_error hist_no_outlier'
        plt.title(title)
        if with_pic ==True:
            plt.savefig('./imgs/%s.png'%title)






    



def map_lanlat(lines, data_path):
    # longitude and latitude should be in decimal format from yaml
    longitude_list = []
    latitude_list = []
    for line in lines: 
        # count += 1
        a=line.split(": ")

        # deal with no-data (default 0) cases
        if a[0] == "longitude":
            if '0.0' not in a[1]:
                longitude_list.append(a[1])
        if a[0] == "latitude":
            if '0.0' not in a[1]:
                latitude_list.append(a[1])
    # print(len(longitude_list))
    # print(len(latitude_list))

    if len(longitude_list) == len(latitude_list):
        
        lonlat_data=np.zeros([len(latitude_list), 2])
        lonlat_data[:,0] = np.array(latitude_list)
        lonlat_data[:,1] = np.array(longitude_list)

        average_x, average_y = lonlat_data.mean(0)
        # range_x = lonlat_data[:,0].max()-lonlat_data[:,0].min()
        # range_y = lonlat_data[:,1].max()-lonlat_data[:,1].min()
        plt.plot(lonlat_data[:,1], lonlat_data[:,0], 'bo')
        # plt.plot(average_x, average_y, "y*")
        
        title = data_path[7:-5]+"\'s static_lonlat_plot"
        plt.title(title)
        #plt.show()

        mplleaflet.show()

    else:
        print("data is incorrect")


if __name__ == "__main__":
    lines = []

    data_path = './gps.yaml'

    #Get from https://www.latlong.net/
    if data_path[-6] == '1':
        moving_1_start_latlon=np.array([42.340439, -71.089133])
        moving_1_end_latlon=np.array([42.336343, -71.099408])

        gt = [moving_1_start_latlon, moving_1_end_latlon]
    elif data_path[-6] == '2':
        moving_2_start_latlon=np.array([42.336129,-71.099252])
        moving_2_end_latlon=np.array([42.340066,-71.089304])
        temp = moving_2_start_latlon-moving_2_end_latlon

        gt = [moving_2_start_latlon, moving_2_end_latlon]
    else:
        gt=[]

    
    with open(data_path) as f:
        lines = f.readlines()

    # plot_utm(lines, data_path, with_plot=False, with_plot_hist=True)
    map_lanlat(lines, data_path)
    # plt.figure()
    #analyze_line(lines, data_path, gt, with_pic=True, use_RANSAC=True)
    #analyze_static(lines)