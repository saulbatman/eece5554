%%%%% Estimate the forward velocity
%%%%%% Magnetometer Calibration

bag = rosbag("./LAB4data/moving.bag");
bSel_imu = select(bag,'Topic','/imu_data');
msgStructs_imu = readMessages(bSel_imu,'DataFormat','struct');
time_imu= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs_imu);
bSel_gps = select(bag,'Topic','/gps_data');
msgStructs_gps = readMessages(bSel_gps,'DataFormat','struct');
time_imu_Sec= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs_imu);
time_imu_Nsec= cellfun(@(m) double(m.Header.Stamp.Nsec), msgStructs_imu);
time_imu=time_imu_Sec+time_imu_Nsec/1e9;
imu_ax= cellfun(@(m) double(m.Imu.LinearAcceleration.X), msgStructs_imu);
imu_ay= cellfun(@(m) double(m.Imu.LinearAcceleration.Y), msgStructs_imu);
imu_az= cellfun(@(m) double(m.Imu.LinearAcceleration.Z), msgStructs_imu);
uncali_imu_vx = cumtrapz(time_imu,imu_ax);
uncali_imu_vy = cumtrapz(time_imu,imu_ay);
imu_vx = cumtrapz(time_imu,imu_ax-0.18);
imu_vy = cumtrapz(time_imu,imu_ay+0.22);

%%%%%------GPS-----%%%%%%
time_gps_Sec= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs_gps);
time_gps_Nsec= cellfun(@(m) double(m.Header.Stamp.Nsec), msgStructs_gps);

time_gps=time_gps_Sec+time_gps_Nsec/1e9;
UtmEasting = cellfun(@(m) double(m.UtmEasting), msgStructs_gps);
UtmNorthing = cellfun(@(m) double(m.UtmNorthing), msgStructs_gps);

[time_gps,gpsIA,gpsIC]=unique(time_gps);
UtmEasting = UtmEasting(gpsIA);
UtmNorthing = UtmNorthing(gpsIA);
delta_easting=diff(UtmEasting);
delta_northing=diff(UtmNorthing);
delta_time=diff(time_gps);
figure
plot(UtmEasting,UtmNorthing);
title("Ground truth from GPS")
gps_vel_easting = delta_easting./delta_time;
gps_vel_northing = delta_northing./delta_time;

% % imu_vz = cumtrapz(time_imu,imu_az);
figure
plot(uncali_imu_vx,'LineWidth',5)
hold on
plot(uncali_imu_vy,'LineWidth',5)
hold on
plot(imu_vx)
hold on
plot(imu_vy)
hold on
% % plot(imu_vz) % gravity
scaling_ratio=size(time_imu)/size(time_gps);
vector=[0:1:size(gps_vel_northing)-1];
plot(vector*scaling_ratio ,gps_vel_easting)
hold on
plot(vector*scaling_ratio ,gps_vel_northing)
title('imu velocity & gps velocity')
legend('uncalibrated imu vx', 'uncalibrated imu vy','imu vx', 'imu vy','gps easting vel','gps northing vel')

