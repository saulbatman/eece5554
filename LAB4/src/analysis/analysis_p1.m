%%%%%% Magnetometer Calibration

bag = rosbag("./LAB4data/circle6.bag");
bSel = select(bag,'Topic','/imu_data');
msgStructs = readMessages(bSel,'DataFormat','struct');
timeSec= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs);
timeNsec= cellfun(@(m) double(m.Header.Stamp.Nsec), msgStructs);
time=timeSec+timeNsec/1e9;
magX = cellfun(@(m) double(m.Magnetic.MagneticField_.X), msgStructs);
magY = cellfun(@(m) double(m.Magnetic.MagneticField_.Y), msgStructs);
magZ = cellfun(@(m) double(m.Magnetic.MagneticField_.Z), msgStructs);
mag = [magX magY magZ];
%%%%------------Magneticmeter correction------------%%%%%
figure
scatter3(mag(:,1),mag(:,2),mag(:,3));
axis equal
hold on;

[A,b,expMFS]  = magcal(mag);
magCorrected = (mag-b)*A;

scatter3(magCorrected(:,1),magCorrected(:,2),magCorrected(:,3));
axis equal
title('Magnetometer Data');
legend('Raw', 'Corrected')
hold off;

t = time-time(1);
%%%%% Calculate the yaw angle from the corrected magnetometer readings.
xdiff = diff(magCorrected(:,1));
ydiff = diff(magCorrected(:,2));
yawMag = atan2d(xdiff,ydiff);
yawMag_filtered = lowpass(yawMag,0.5,40);

%%%%% Integrate the yaw rate sensor to get yaw angle.
angVX = cellfun(@(m) double(m.Imu.AngularVelocity.X), msgStructs);
angVY = cellfun(@(m) double(m.Imu.AngularVelocity.Y), msgStructs);
angVZ = cellfun(@(m) double(m.Imu.AngularVelocity.Z), msgStructs);
angX = cumtrapz(t, angVX);
angY = cumtrapz(t, angVY);
angZ = cumtrapz(t, angVZ);
angX = -rad2deg(angX);
angY = -rad2deg(angY);
angZ = -rad2deg(angZ);
% % % figure
% % % plot(angX)
% % % hold on;
% % % plot(angY)
% % % hold on;
% % % plot(angZ)
% % % hold on;
% % % legend('angX','angY','angZ')


yawIMU_filtered = highpass(angZ,18,40);
%%%%------------Magneticmeter and IMU YAW------------%%%%%
figure
plot(yawMag);
hold on;
plot(yawIMU);
hold on;
plot(yawMag_filtered,'LineWidth',5);
hold on;
plot(yawIMU_filtered,'LineWidth',5);
title('Comparing yaw between Magneticmeter and IMU')
legend('Mag','IMU','yawMag filtered','yawIMU filtered')
hold off;
%%%% Use a complementary filter to combine the measurements from the magnetometer and yaw rate
aX = cellfun(@(m) double(m.Imu.LinearAcceleration.X), msgStructs);
aY = cellfun(@(m) double(m.Imu.LinearAcceleration.Y), msgStructs);
aZ = cellfun(@(m) double(m.Imu.LinearAcceleration.Z), msgStructs);
%% test the accelaration bias
% % % X = cumtrapz(t, aX-0.11);
% % % Y = cumtrapz(t, aY+0.29);
% % % Z = cumtrapz(t, aZ+9.807);
% % % plot(X)
% % % hold on;
% % % plot(Y)
% % % hold on;
% % % plot(Z)
% % % hold on;
% % % legend('x','y','z')
fuse = complementaryFilter('SampleRate', 40);
[orientation,angularVelocity] = fuse([aX-0.11,aY+0.29,aZ], [angVX,angVY,angVZ], magCorrected);
degreeXYZ=unwrap(eulerd(orientation, 'ZYX', 'frame'));
%%%%%%--------fuse and IMU yaw integration---------%%%%%%%%
figure
plot(degreeXYZ);
title('Orientation Estimate');
ylabel('Degrees');
disp(max(degreeXYZ(:,1))/360);
hold on;
plot(angZ,'--')
hold on;
plot(angY,'--')
hold on;
plot(angX,'--')
title('Comparing ZYX angle integration of fuse data and IMU data')
legend('Fuse Z-rotation', 'Fuse Y-rotation', 'Fuse X-rotation', 'IMU Z-rotation', 'IMU Y-rotation', 'IMU X-rotation')
hold off;

%%%%%%--------fuse and IMU yaw---------%%%%%%%%
figure
plot(diff(degreeXYZ(:,1)),'b')
hold on;
plot(diff(angZ),'r')
title('Comparing yaw of fuse data and IMU data')
legend('fuse yaw','imu yaw')