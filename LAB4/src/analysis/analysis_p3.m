%%%%% Integrate IMU data to obtain displacement and compare with GPS.
%%%%%% Magnetometer Calibration

bag = rosbag("./LAB4data/moving.bag");
bSel_imu = select(bag,'Topic','/imu_data');
msgStructs_imu = readMessages(bSel_imu,'DataFormat','struct');
bSel_gps = select(bag,'Topic','/gps_data');
msgStructs_gps = readMessages(bSel_gps,'DataFormat','struct');
%%%%%------GPS-----%%%%%%
time_gps_Sec= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs_gps);
time_gps_Nsec= cellfun(@(m) double(m.Header.Stamp.Nsec), msgStructs_gps);

time_gps=time_gps_Sec+time_gps_Nsec/1e9;
UtmEasting = cellfun(@(m) double(m.UtmEasting), msgStructs_gps);
UtmNorthing = cellfun(@(m) double(m.UtmNorthing), msgStructs_gps);
[time_gps,gpsIA,gpsIC]=unique(time_gps);
UtmEasting = UtmEasting(gpsIA);
UtmNorthing = UtmNorthing(gpsIA);
delta_easting=diff(UtmEasting);
delta_northing=diff(UtmNorthing);
delta_time=diff(time_gps);
gps_vel_easting = delta_easting./delta_time;
gps_vel_northing = delta_northing./delta_time;

time_imu_Sec= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs_imu);
time_imu_Nsec= cellfun(@(m) double(m.Header.Stamp.Nsec), msgStructs_imu);
time_imu=time_imu_Sec+time_imu_Nsec/1e9;
imu_ax= cellfun(@(m) double(m.Imu.LinearAcceleration.X), msgStructs_imu);
imu_ay= cellfun(@(m) double(m.Imu.LinearAcceleration.Y), msgStructs_imu);
imu_az= cellfun(@(m) double(m.Imu.LinearAcceleration.Z), msgStructs_imu);

magX = cellfun(@(m) double(m.Magnetic.MagneticField_.X), msgStructs_imu);
magY = cellfun(@(m) double(m.Magnetic.MagneticField_.Y), msgStructs_imu);
magZ = cellfun(@(m) double(m.Magnetic.MagneticField_.Z), msgStructs_imu);
new_yaw=atan2d(magX,magY);
new_yaw = lowpass(new_yaw,0.5,40);

w_list= cellfun(@(m) double(m.Imu.AngularVelocity.Z), msgStructs_imu);
wdot = diff(w_list);
wdot_list = [wdot; wdot(end)];
wdot=zeros(size(wdot_list,1),3);
wdot(:,3)=wdot_list;
w=zeros(size(w_list,1),3);
w(:,3)=w_list;
yaw=cumtrapz(time_imu, w_list);

uncali_Xdot = cumtrapz(time_imu,imu_ax);
uncali_Ydot = cumtrapz(time_imu,imu_ay);
Xdot = cumtrapz(time_imu,imu_ax-0.18);
Ydot = cumtrapz(time_imu,imu_ay+0.22);
tmp=zeros(size(Xdot,1),1);
XYdot = [Xdot Ydot tmp];

wXdot=w.*Xdot;
ydot2=imu_ay;

figure
plot(lowpass(wXdot,5,40))
hold on;
plot(lowpass(ydot2,5,40))
tit1=title('Comparing $w\dot{X}$ and $\ddot{y}$');
leg1=legend('$w\dot{X}$','$\ddot{y}$');
set(leg1,'Interpreter','latex');
set(tit1,'Interpreter','latex');

%ddot {x} = dot {v} +ω×v= ddot {X} + dot {ω} ×r+ω× dot {X} +ω×(ω×r)
tmp=zeros(size(imu_az,1),1);
Xddot=[imu_ax imu_ay];
Yddot=wXdot;


imu_x=0;
imu_y=0;

for i = 2:size(Xdot)
    if yaw(i) > -1.5707963268
        imu_x(i) = imu_x(i-1) + cos(-yaw(i)) * Xdot(i-1) * dt;
        imu_y(i) = imu_y(i-1) + sin(-yaw(i)) * Xdot(i-1) * dt;
    end
    if (yaw(i) < -1.5707963268)
        if yaw(i) > -3.14
            imu_x(i) = imu_x(i-1) + cos(-yaw(i)+pi) * Xdot(i-1) * dt;
            imu_y(i) = imu_y(i-1) + sin(-yaw(i)+pi) * Xdot(i-1) * dt;
        end
        if yaw(i) < -3.14
            imu_x(i) = imu_x(i-1) + cos(-yaw(i)+pi) * Xdot(i-1) * dt;
            imu_y(i) = imu_y(i-1) + sin(-yaw(i)+pi) * Xdot(i-1) * dt;
        end
    end
end


for i = 2:size(Xdot)
    if yaw(i) > 0
        imu_x(i) = imu_x(i-1) + cos(-yaw(i)) * Xdot(i-1) * dt;
        imu_y(i) = imu_y(i-1) + sin(-yaw(i)) * Xdot(i-1) * dt;
    end
    if (yaw(i) < 0)
        imu_x(i) = imu_x(i-1) + cos(-yaw(i)-pi) * Xdot(i-1) * dt;
        imu_y(i) = imu_y(i-1) + sin(-yaw(i)-pi) * Xdot(i-1) * dt;
    end
end


figure
dt=1/40;


theta_vec = -0.6*pi;
R_estimate2gps = [cos(theta_vec), sin(theta_vec);
                  -sin(theta_vec),cos(theta_vec)];
imu_xy = 1.2 * R_estimate2gps * [imu_x; imu_y];
imu_xy=imu_xy';


idx=1;
idx1=floor(size(imu_xy,1));
plot(imu_xy(idx:idx1,1)-imu_xy(idx,1),imu_xy(idx:idx1,2)--imu_xy(idx,2),'--')
hold on;
scaling_ratio=size(time_imu)/size(time_gps);
vector=[0:1:size(gps_vel_easting)-1];
idx2=floor(size(UtmEasting,1));
plot(UtmEasting(1:idx2)-UtmEasting(1), UtmNorthing(1:idx2)-UtmNorthing(1));
legend("Estimation","Ground truth from GPS")



xc=mean((ydot2-wXdot(:,3)))