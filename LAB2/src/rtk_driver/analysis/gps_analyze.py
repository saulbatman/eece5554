from cProfile import label
from turtle import width
import numpy as np
import matplotlib.pylab as plt
import mplleaflet
from sklearn import linear_model
import utm
# count = 0



def plot_utm(lines, data_path, with_plot=True, with_plot_hist=False):
    utm_easting_list = []
    utm_northing_list = []
    altitude_list = []
    for line in lines: 
        # count += 1
        line = line.strip()
        a=line.split(": ")
        if a[0] == "utm_easting":
            utm_easting_list.append(a[1])
        if a[0] == "utm_northing":
            utm_northing_list.append(a[1])
        if a[0] == "altitude":
            altitude_list.append(a[1])

    if len(utm_easting_list) == len(utm_northing_list):

        utm_easting_list = np.array(utm_easting_list).astype(float)
        utm_northing_list = np.array(utm_northing_list).astype(float)
        altitude_list = np.array(altitude_list).astype(float)

        utm_data=np.zeros([len(utm_northing_list), 2])
        utm_data[:,0] = utm_easting_list
        utm_data[:,1] = utm_northing_list


        average_x, average_y = utm_data.mean(0)
        range_x = utm_data[:,0].max()-utm_data[:,0].min()
        range_y = utm_data[:,1].max()-utm_data[:,1].min()



        # centerized plot
        plt.figure()
        plt.scatter(utm_data[:,0]-average_x, utm_data[:,1]-average_y, marker='.', label="data points")
        plt.scatter(0, 0, marker='*', label="average point")

        plt.grid()
        title = data_path[7:-5]+"_static_utm_plot_centered"
        plt.title(title)
        plt.legend()
        plt.xlabel("utm_easting")
        plt.ylabel("utm_northing")
        if with_plot==True:
            plt.savefig('./imgs/%s.png'%title)



        # # directly plot
        # plt.figure()
        # plt.scatter(utm_data[:,0], utm_data[:,1], marker=".", label="data points")
        # plt.scatter(average_x, average_y, marker="*", label="average point")

        # plt.grid()
        # title = data_path[7:-5]+"_utm_plot_orin"
        # plt.title(title)
        # plt.legend()
        # if with_plot==True:
        #     plt.savefig('./imgs/%s.png'%title)



        # plot altitude
        plt.figure()
        altitude_list = np.array(altitude_list).astype(float)
        print(altitude_list)
        plt.plot(altitude_list, label="data points")
        plt.xlabel("time")
        plt.ylabel("altitude")
        # plt.hist(altitude_list-altitude_list.mean())

        # plt.yticks(altitude_list[::600])
        plt.grid()
        
        plt.title(title)
        plt.legend()
        if with_plot==True:
            title = data_path[7:-5]+"_altitude_plot"
            plt.savefig('./imgs/%s.png'%title)

        plt.figure()
        plt.hist(altitude_list-altitude_list.mean(), bins=30)
        if with_plot_hist==True:
            title = data_path[7:-5]+"_altitude_hist"
            plt.savefig('./imgs/%s.png'%title)



        # data analysis
        variance_2d = np.std(utm_data)

        # data_report = {"range_easting: ": range_x,
        #                "range_northing: ": range_y,
        #                "2d variance: ": variance_2d,
        #                "average utm coordinate: ": (average_x, average_y),}

        # print(data_report)
        if with_plot_hist==True:
            average_x, average_y = utm_data.mean(0)
            avg_data = np.array([average_x, average_y])
            # distances = np.linalg.norm(utm_data-avg_data, axis=1)
            # distances = (utm_data-avg_data)
            centered_data = (utm_data-avg_data).astype(float)
            distance_list=np.array([])
            for i in range(len(centered_data)):
                distances = np.linalg.norm([centered_data[i]], ord=1,axis=1)
                
                if centered_data[i,0]<0:
                    distance_list = np.append(distance_list, -distances)
                else:
                    distance_list = np.append(distance_list, distances)
            print('easting_range:', utm_easting_list.max()-utm_easting_list.min())
            print('northing_range:', utm_northing_list.max()-utm_northing_list.min())   
            print("std variance: ", np.std(centered_data))    
            plt.figure()
            plt.hist(distance_list-distance_list.mean(), bins=30)
            title = data_path[7:-5]+"_position_error_hist"
            plt.savefig('./imgs/%s.png'%title)
            
        # plt.show()
        

    else:
        print("data is incorrect")
        
def analyze_static(lines):
    utm_easting_list = []
    utm_northing_list = []
    altitude_list = []
    for line in lines: 
        # count += 1
        line = line.strip()
        a=line.split(": ")
        if a[0] == "utm_easting":
            utm_easting_list.append(a[1])
        if a[0] == "utm_northing":
            utm_northing_list.append(a[1])
        if a[0] == "altitude":
            altitude_list.append(a[1])

    if len(utm_easting_list) == len(utm_northing_list):
        utm_easting_list = np.array(utm_easting_list).astype(float)
        utm_northing_list = np.array(utm_northing_list).astype(float)
        altitude_list = np.array(altitude_list).astype(float)

        utm_data=np.zeros([len(utm_northing_list), 2])
        utm_data[:,0] = utm_easting_list
        utm_data[:,1] = utm_northing_list


        average_x, average_y = utm_data.mean(0)
        avg_data = np.array([average_x, average_y])
        # distances = np.linalg.norm(utm_data-avg_data, axis=1)
        # distances = (utm_data-avg_data)
        centered_data = (utm_data-avg_data).astype(float)
        distance_list=np.array([])
        for i in range(len(centered_data)):
            distances = np.linalg.norm([centered_data[i]], ord=1,axis=1)
            
            if centered_data[i,0]<0:
                distance_list = np.append(distance_list, -distances)
            else:
                distance_list = np.append(distance_list, distances)
        print('easting_range:', utm_easting_list.max()-utm_easting_list.min())
        print('northing_range:', utm_northing_list.max()-utm_northing_list.min())   
        print("std variance: ", np.std(centered_data))    
        plt.hist(distance_list-distance_list.mean())


        plt.show()





def analyze_line(lines, data_path, with_pic=True, use_RANSAC=False, location="isec"):
    print("analyzing: ", location)
    utm_easting_list = []
    utm_northing_list = []
    altitude_list = []
    quality_list = []
    for line in lines: 
        # count += 1
        line = line.strip()
        a=line.split(": ")
        if a[0] == "utm_easting":
            utm_easting_list.append(a[1])
        if a[0] == "utm_northing":
            utm_northing_list.append(a[1])
        if a[0] == "altitude":
            altitude_list.append(a[1])
        if a[0] == "quality":
            quality_list.append(a[1])

    



    if len(utm_easting_list) == len(utm_northing_list):
        # print(moving_utm)
        # print(utm_easting_list[0], utm_northing_list[0])

        utm_easting_list = np.array(utm_easting_list).reshape(-1,1).astype(float)
        utm_northing_list = np.array(utm_northing_list).reshape(-1,1).astype(float)
        quality_list = np.array(quality_list).astype(int)

        utm_easting_list = utm_easting_list[quality_list>=4]
        utm_northing_list = utm_northing_list[quality_list>=4]
        print("delete \"quality=1\" points: %s / %s"%((quality_list<4).sum(), len(quality_list)))

        if 'isec' in location:
            # Original plot
            plt.figure()
            plt.plot(utm_easting_list[400:], utm_northing_list[400:], "go", label="data")
            title=data_path[7:-5]+'_position_plot'
            plt.title(title)
            p1=[328087, 4689325]
            p2=[328107, 4689336]
            p3=[328101, 4689347]
            p4=[328084, 4689337]
            p5=p1
            points= np.array([p1,p2,p3,p4,p5])
            print(points)
            plt.plot(points[:,0], points[:,1],label="ground truth")

            

            a1=400
            a2=1650

            b1=a2
            b2=2200

            c1=b2
            c2=3000

            d1=3000
            plt.plot(utm_easting_list[a1:a2], utm_northing_list[a1:a2], label="trajectory1")
            plt.plot(utm_easting_list[b1:b2], utm_northing_list[b1:b2], label="trajectory2")
            plt.plot(utm_easting_list[c1:c2], utm_northing_list[c1:c2], label="trajectory3")
            plt.plot(utm_easting_list[d1:], utm_northing_list[d1:], label="trajectory4")
            title=data_path[7:-5]+'_position_plot'

            # k1,bias1 = np.polyfit((p1[0], p2[0]),(p1[1], p2[1]),1)
            # h1 = [utm_easting_list[a1], (k1*utm_easting_list[a1]+bias1)]
            # h2 = [utm_easting_list[a2], (k1*utm_easting_list[a2]+bias1)]
            # hh = np.array([h1,h2])
            # plt.plot(hh[:,0], hh[:,1], label="fit")

            plt.title(title)
            plt.legend()
            plt.xlabel("utm_easting")
            plt.ylabel("utm_northing")
            plt.savefig('./imgs/%s.png'%title)
            #plt.show()

            plt.figure()
            k1,bias1 = np.polyfit((p1[0], p2[0]),(p1[1], p2[1]),1)
            error1 = k1*utm_easting_list[a1:a2]+bias1-utm_northing_list[a1:a2]
            k2,bias2 = np.polyfit((p2[0], p3[0]),(p2[1], p3[1]),1)
            error2 = k2*utm_easting_list[b1:b2]+bias2-utm_northing_list[b1:b2]
            k3,bias3 = np.polyfit((p3[0], p4[0]),(p3[1], p4[1]),1)
            error3 = k3*utm_easting_list[c1:c2]+bias3-utm_northing_list[c1:c2]
            k4, bias4 = np.polyfit((p4[0], p4[0]),(p5[1], p5[1]),1)
            error4 = k4*utm_easting_list[d1:]+bias4-utm_northing_list[d1:]
            error_all=np.array([])
            error_all=np.append(error_all, error1)
            error_all=np.append(error_all, error2)
            error_all=np.append(error_all, error3)
            error_all=np.append(error_all, error4)

            plt.hist(error_all,bins=20)
            title=data_path[7:-5]+'_error_hist'
            plt.title(title)
            plt.savefig('./imgs/%s.png'%title)


        else:
             # Original plot
            plt.figure()
            plt.plot(utm_easting_list[400:], utm_northing_list[400:], "go", label="data")
            title=data_path[7:-5]+'_position_plot'
            plt.title(title)
            
            p1=[328315, 4689587]
            p2=[328299, 4689570]
            p3=[328309, 4689560]
            p4=[328327, 4689578]
            p5=p1
            points= np.array([p1,p2,p3,p4,p5])
            print(points)
            plt.plot(points[:,0], points[:,1],label="ground truth")
        

            

            a1=0
            a2=2250

            b1=a2
            b2=3250

            c1=b2
            c2=4850

            d1=c2
            plt.plot(utm_easting_list[a1:a2], utm_northing_list[a1:a2], label="trajectory1")
            plt.plot(utm_easting_list[b1:b2], utm_northing_list[b1:b2], label="trajectory2")
            plt.plot(utm_easting_list[c1:c2], utm_northing_list[c1:c2], label="trajectory3")
            plt.plot(utm_easting_list[d1:], utm_northing_list[d1:], label="trajectory4")
            title=data_path[7:-5]+'_position_plot'

            # k1,bias1 = np.polyfit((p1[0], p2[0]),(p1[1], p2[1]),1)
            # h1 = [utm_easting_list[a1], (k1*utm_easting_list[a1]+bias1)]
            # h2 = [utm_easting_list[a2], (k1*utm_easting_list[a2]+bias1)]
            # hh = np.array([h1,h2])
            # plt.plot(hh[:,0], hh[:,1], label="fit")

            plt.title(title)
            plt.legend()
            plt.xlabel("utm_easting")
            plt.ylabel("utm_northing")
            plt.savefig('./imgs/%s.png'%title)
            # plt.show()

            plt.figure()
            k1,bias1 = np.polyfit((p1[0], p2[0]),(p1[1], p2[1]),1)
            error1 = k1*utm_easting_list[a1:a2]+bias1-utm_northing_list[a1:a2]
            k2,bias2 = np.polyfit((p2[0], p3[0]),(p2[1], p3[1]),1)
            error2 = k2*utm_easting_list[b1:b2]+bias2-utm_northing_list[b1:b2]
            k3,bias3 = np.polyfit((p3[0], p4[0]),(p3[1], p4[1]),1)
            error3 = k3*utm_easting_list[c1:c2]+bias3-utm_northing_list[c1:c2]
            k4, bias4 = np.polyfit((p4[0], p4[0]),(p5[1], p5[1]),1)
            error4 = k4*utm_easting_list[d1:]+bias4-utm_northing_list[d1:]
            error_all=np.array([])
            error_all=np.append(error_all, error1)
            error_all=np.append(error_all, error2)
            error_all=np.append(error_all, error3)
            error_all=np.append(error_all, error4)

            plt.hist(error_all,bins=20)
            title=data_path[7:-5]+'_error_hist'
            plt.title(title)
            plt.savefig('./imgs/%s.png'%title)


        


        # ####**************Ref:Sci-learn Doc****************####
        # ####https://scikit-learn.org/stable/auto_examples/linear_model/plot_ransac.html
        # # Fit line using all data
        # lr = linear_model.LinearRegression()
        # lr.fit(utm_easting_list, utm_northing_list)
        # # Predict data of estimated models
        # line_X = np.arange(utm_easting_list.min(), utm_easting_list.max())[:, np.newaxis]
        # line_y = lr.predict(line_X)

        # if use_RANSAC==True:
        #     # Robustly fit linear model with RANSAC algorithm
        #     residual_threshold = np.median(np.abs(utm_northing_list - np.median(utm_northing_list)))*0.5
        #     ransac = linear_model.RANSACRegressor(residual_threshold=residual_threshold)
        #     ransac.fit(utm_easting_list, utm_northing_list)
        #     inlier_mask = ransac.inlier_mask_
        #     outlier_mask = np.logical_not(inlier_mask)
        #     line_y_ransac = ransac.predict(line_X)
        #     plt.scatter(
        #         utm_easting_list[inlier_mask], utm_northing_list[inlier_mask], color="yellowgreen", label="Inliers"
        #     )
        #     plt.scatter(
        #         utm_easting_list[outlier_mask], utm_northing_list[outlier_mask], color="gold",  label="Outliers"
        #     )
        # lw = 2
        
        # if use_RANSAC ==False:
        #     plt.scatter(
        #         utm_easting_list, utm_northing_list, color="gold", marker=".", label="position"
        #     )

        # plt.plot(line_X, line_y, color="navy", linewidth=lw, label="Linear regressor")
        # plt.plot(moving_utm[:, 0], moving_utm[:,1], color='g', label='ground_truth', linewidth=lw)
        # plt.plot(
        #     line_X,
        #     line_y_ransac,
        #     color="cornflowerblue",
        #     linewidth=lw,
        #     label="RANSAC regressor",
        # )
        # ####**********************************************###
        # # print()
        # slope_lr = (lr.predict([utm_easting_list[0]])-lr.predict([utm_easting_list[10]]))/(utm_easting_list[0]-utm_easting_list[10])
        # print(slope_lr)
        # bias_lr = lr.predict([utm_easting_list[0]])-slope_lr*utm_easting_list[0]
        # # Calculation MSE Error
        # predicted_northing = lr.predict(utm_easting_list)
        # L1Error = np.mean(abs(predicted_northing - utm_northing_list))
        # L2Error = np.mean((predicted_northing - utm_northing_list)**2)

        # print('easting_range:', utm_easting_list.max()-utm_easting_list.min())
        # print('northing_range:', utm_northing_list.max()-utm_northing_list.min())
        # print("L1Error: ", L1Error)
        # print("L2Error: ", L2Error)


        # if use_RANSAC==True:
        #     slope_ransac = (ransac.predict([utm_easting_list[0]])-ransac.predict([utm_easting_list[10]]))/(utm_easting_list[0]-utm_easting_list[10])
        #     print(slope_ransac)
        #     bias_ransac = ransac.predict([utm_easting_list[0]])-slope_ransac*utm_easting_list[0]




        # # plot
        # plt.legend()
        # title = data_path[7:-5]+"_fit_line"
        # plt.title(title)
        # if with_pic == True:
        #     plt.savefig('./imgs/%s.png'%title)

        # plt.figure()
        # histerror = predicted_northing[np.invert(outlier_mask)] - utm_northing_list[np.invert(outlier_mask)]
        # plt.hist(histerror)
        # title=data_path[7:-5]+'_error hist_no_outlier'
        # plt.title(title)
        # if with_pic ==True:
        #     plt.savefig('./imgs/%s.png'%title)






    



def map_lanlat(lines, data_path):
    # longitude and latitude should be in decimal format from yaml
    longitude_list = []
    latitude_list = []
    for line in lines: 
        # count += 1
        a=line.split(": ")

        # deal with no-data (default 0) cases
        if a[0] == "longitude":
            if '0.0' not in a[1]:
                longitude_list.append(a[1])
        if a[0] == "latitude":
            if '0.0' not in a[1]:
                latitude_list.append(a[1])
    # print(len(longitude_list))
    # print(len(latitude_list))

    if len(longitude_list) == len(latitude_list):
        
        lonlat_data=np.zeros([len(latitude_list), 2])
        lonlat_data[:,0] = np.array(latitude_list)
        lonlat_data[:,1] = np.array(longitude_list)

        average_x, average_y = lonlat_data.mean(0)
        # range_x = lonlat_data[:,0].max()-lonlat_data[:,0].min()
        # range_y = lonlat_data[:,1].max()-lonlat_data[:,1].min()
        plt.plot(lonlat_data[:,1], lonlat_data[:,0], 'bo')
        # plt.plot(average_x, average_y, "y*")
        
        title = data_path[7:-5]+"\'s static_lonlat_plot"
        plt.title(title)

        # plt.show()

        mplleaflet.show()

    else:
        print("data is incorrect")


if __name__ == "__main__":
    lines = []

    data_path = './data/tennis_moving.yaml'

    
    with open(data_path) as f:
        lines = f.readlines()

    plot_utm(lines, data_path, with_plot=True, with_plot_hist=True)
    #map_lanlat(lines, data_path)
    # plt.figure()
    #analyze_line(lines, data_path, with_pic=True, use_RANSAC=True,location=data_path)
    #analyze_static(lines)