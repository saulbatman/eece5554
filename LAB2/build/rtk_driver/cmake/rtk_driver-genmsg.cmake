# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "rtk_driver: 1 messages, 0 services")

set(MSG_I_FLAGS "-Irtk_driver:/home/mingxi/eece5554/LAB2/src/rtk_driver/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(rtk_driver_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg" NAME_WE)
add_custom_target(_rtk_driver_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rtk_driver" "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg" "std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(rtk_driver
  "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtk_driver
)

### Generating Services

### Generating Module File
_generate_module_cpp(rtk_driver
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtk_driver
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(rtk_driver_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(rtk_driver_generate_messages rtk_driver_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg" NAME_WE)
add_dependencies(rtk_driver_generate_messages_cpp _rtk_driver_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtk_driver_gencpp)
add_dependencies(rtk_driver_gencpp rtk_driver_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtk_driver_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(rtk_driver
  "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtk_driver
)

### Generating Services

### Generating Module File
_generate_module_eus(rtk_driver
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtk_driver
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(rtk_driver_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(rtk_driver_generate_messages rtk_driver_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg" NAME_WE)
add_dependencies(rtk_driver_generate_messages_eus _rtk_driver_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtk_driver_geneus)
add_dependencies(rtk_driver_geneus rtk_driver_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtk_driver_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(rtk_driver
  "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtk_driver
)

### Generating Services

### Generating Module File
_generate_module_lisp(rtk_driver
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtk_driver
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(rtk_driver_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(rtk_driver_generate_messages rtk_driver_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg" NAME_WE)
add_dependencies(rtk_driver_generate_messages_lisp _rtk_driver_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtk_driver_genlisp)
add_dependencies(rtk_driver_genlisp rtk_driver_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtk_driver_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(rtk_driver
  "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtk_driver
)

### Generating Services

### Generating Module File
_generate_module_nodejs(rtk_driver
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtk_driver
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(rtk_driver_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(rtk_driver_generate_messages rtk_driver_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg" NAME_WE)
add_dependencies(rtk_driver_generate_messages_nodejs _rtk_driver_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtk_driver_gennodejs)
add_dependencies(rtk_driver_gennodejs rtk_driver_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtk_driver_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(rtk_driver
  "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtk_driver
)

### Generating Services

### Generating Module File
_generate_module_py(rtk_driver
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtk_driver
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(rtk_driver_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(rtk_driver_generate_messages rtk_driver_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mingxi/eece5554/LAB2/src/rtk_driver/msg/gps.msg" NAME_WE)
add_dependencies(rtk_driver_generate_messages_py _rtk_driver_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtk_driver_genpy)
add_dependencies(rtk_driver_genpy rtk_driver_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtk_driver_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtk_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtk_driver
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(rtk_driver_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtk_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtk_driver
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(rtk_driver_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtk_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtk_driver
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(rtk_driver_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtk_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtk_driver
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(rtk_driver_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtk_driver)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtk_driver\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtk_driver
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(rtk_driver_generate_messages_py std_msgs_generate_messages_py)
endif()
