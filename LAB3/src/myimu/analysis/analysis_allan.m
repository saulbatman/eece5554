% bag = rosbag("mydata.bag")
% bSel = select(bag,'Topic','/imu_data')
% 
% msgStructs = readMessages(bSel,'DataFormat','struct');

maxNumM = 100;
L = size(msgStructs, 1);
maxM = 2.^floor(log2(L/2));
m = logspace(log10(1), log10(maxM), maxNumM).';
m = ceil(m); % m must be an integer.
m = unique(m); % Remove duplicates.

frequency=40;
t0=1/frequency;
tau = m*t0;
frequency=40;
t0=1/frequency;
tau = m*t0;


%------------YPR------------%

% Yaw = cellfun(@(m) double(m.Ypr.X), msgStructs);
% Pitch = cellfun(@(m) double(m.Ypr.Y), msgStructs);
% Roll = cellfun(@(m) double(m.Ypr.Z), msgStructs);
% MagneticX = cellfun(@(m) double(m.Magnetic.MagneticField_.X), msgStructs);
% MagneticY = cellfun(@(m) double(m.Magnetic.MagneticField_.Y), msgStructs);
% MagneticZ = cellfun(@(m) double(m.Magnetic.MagneticField_.Z), msgStructs);
% AngularVelocityX = cellfun(@(m) double(m.Imu.AngularVelocity.X), msgStructs);
% AngularVelocityY = cellfun(@(m) double(m.Imu.AngularVelocity.Y), msgStructs);
% AngularVelocityZ = cellfun(@(m) double(m.Imu.AngularVelocity.Z), msgStructs);
% LinearAccelerationX = cellfun(@(m) double(m.Imu.LinearAcceleration.X), msgStructs);
% LinearAccelerationY = cellfun(@(m) double(m.Imu.LinearAcceleration.Y), msgStructs);
% LinearAccelerationZ = cellfun(@(m) double(m.Imu.LinearAcceleration.Z), msgStructs);
% QuaternionX = cellfun(@(m) double(m.Imu.Orientation.X), msgStructs);
% QuaternionY = cellfun(@(m) double(m.Imu.Orientation.Y), msgStructs);
% QuaternionZ = cellfun(@(m) double(m.Imu.Orientation.Z), msgStructs);
% QuaternionW = cellfun(@(m) double(m.Imu.Orientation.W), msgStructs);

data=MagneticX;
data_type = "MagneticX";
theta = cumsum(data, 1)*t0;

type="all";
[avar, adev] = alan_variance(data, L,m,tau);
plot_allan(adev,L,m,tau,type)
title('Alan Variance with Noise Parameters for '+data_type)

