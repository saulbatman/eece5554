% bag = rosbag("isec_data.bag");
% bSel = select(bag,'Topic','/imu_data');
% 
% msgStructs = readMessages(bSel,'DataFormat','struct');
% time= cellfun(@(m) double(m.Header.Stamp.Sec), msgStructs);
% 
% Yaw = cellfun(@(m) double(m.Ypr.X), msgStructs);
% Pitch = cellfun(@(m) double(m.Ypr.Y), msgStructs);
% Roll = cellfun(@(m) double(m.Ypr.Z), msgStructs);
% MagneticX = cellfun(@(m) double(m.Magnetic.MagneticField_.X), msgStructs);
% MagneticY = cellfun(@(m) double(m.Magnetic.MagneticField_.Y), msgStructs);
% MagneticZ = cellfun(@(m) double(m.Magnetic.MagneticField_.Z), msgStructs);
% AngularVelocityX = cellfun(@(m) double(m.Imu.AngularVelocity.X), msgStructs);
% AngularVelocityY = cellfun(@(m) double(m.Imu.AngularVelocity.Y), msgStructs);
% AngularVelocityZ = cellfun(@(m) double(m.Imu.AngularVelocity.Z), msgStructs);
% LinearAccelerationX = cellfun(@(m) double(m.Imu.LinearAcceleration.X), msgStructs);
% LinearAccelerationY = cellfun(@(m) double(m.Imu.LinearAcceleration.Y), msgStructs);
% LinearAccelerationZ = cellfun(@(m) double(m.Imu.LinearAcceleration.Z), msgStructs);
% QuaternionX = cellfun(@(m) double(m.Imu.Orientation.X), msgStructs);
% QuaternionY = cellfun(@(m) double(m.Imu.Orientation.Y), msgStructs);
% QuaternionZ = cellfun(@(m) double(m.Imu.Orientation.Z), msgStructs);
% QuaternionW = cellfun(@(m) double(m.Imu.Orientation.W), msgStructs);

figure
data=AngularVelocityZ;
data_type='AngularVelocityZ';
disp(data_type)
myplot=plot(time-time(1),data);
hold on
myMean =mean(data)
plot(time-time(1), ones(size(data,1),1)*myMean);
mySTD = std(data)
xlabel("time")
ylabel("rad/s")
title("time-"+data_type+" plot")
legend('data', 'mean')
% text(time(end)-time(1)-150,min(data)+0.001,{'mean='+string(myMean),'std='+string(mySTD)})
saveas(gcf,"./imgs/individual/"+data_type+'.jpg')