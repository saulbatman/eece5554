function plot_allan(adev, L,m,tau, type)

    
    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = -0.5;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));
    
    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);
    
    % Determine the angle random walk coefficient from the line.
    logN = slope*log(1) + b;
    N = 10^logN;
    
    % Plot the results.
    tauN = 1;
    lineN = N ./ sqrt(tau);
    if type == "random walk"
        figure
        loglog(tau, adev, tau, lineN, '--', tauN, N, 'o')
        title('Allan Deviation with Angle Random Walk')
        xlabel('\tau')
        ylabel('\sigma(\tau)')
        legend('\sigma', '\sigma_N')
        text(tauN, N, 'N')
        grid on
        axis equal
    end



    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = 0.5;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));
    
    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);
    
    % Determine the rate random walk coefficient from the line.
    logK = slope*log10(3) + b;
    K = 10^logK;
    
    % Plot the results.
    tauK = 3;
    lineK = K .* sqrt(tau/3);
    if type == "rate random walk"
        figure
        loglog(tau, adev, tau, lineK, '--', tauK, K, 'o')
        title('Allan Deviation with Rate Random Walk')
        xlabel('\tau')
        ylabel('\sigma(\tau)')
        legend('\sigma', '\sigma_K')
        text(tauK, K, 'K')
        grid on
        axis equal
    end
    
    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = 0;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));
    
    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);
    
    % Determine the bias instability coefficient from the line.
    scfB = sqrt(2*log(2)/pi);
    logB = b - log10(scfB);
    B = 10^logB;
    
    % Plot the results.
    tauB = tau(i);
    lineB = B * scfB * ones(size(tau));
    if type=="bias instability"

        figure
        loglog(tau, adev, tau, lineB, '--', tauB, scfB*B, 'o')
        title('Allan Deviation with Bias Instability')
        xlabel('\tau')
        ylabel('\sigma(\tau)')
        legend('\sigma', '\sigma_B')
        text(tauB, scfB*B, '0.664B')
        grid on
        axis equal
    end
    
    if type=="all"
        tauParams = [tauN, tauK, tauB];
        params = [N, K, scfB*B];
        figure
        loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
            tauParams, params, 'o')
        title('Allan Deviation with Noise Parameters')
        xlabel('\tau')
        ylabel('\sigma(\tau)')
        legend('$\sigma $', '$\sigma_N $', ...
            '$\sigma_K $', '$\sigma_B $', 'Interpreter', 'latex')
        text(tauParams, params, {'N', 'K', '0.664B'})
        grid on
        axis equal
    end
end