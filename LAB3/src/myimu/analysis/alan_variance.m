
function [avar, adev] = alan_variance(data, L,m,tau)

    
    avar = zeros(numel(m), 1);
    for i = 1:numel(m)
        mi = m(i);
        avar(i,:) = sum( ...
            (data(1+2*mi:L) - 2*data(1+mi:L-mi) + data(1:L-2*mi)).^2, 1);
    end
    avar = avar ./ (2*tau.^2 .* (L - 2*m));
    
    adev = sqrt(avar);
    
    

    



end