(cl:in-package imu_driver-msg)
(cl:export '(HEADER-VAL
          HEADER
          CHILD_FRAME_ID-VAL
          CHILD_FRAME_ID
          YPR-VAL
          YPR
          MAGNETIC-VAL
          MAGNETIC
          ACCELERATION-VAL
          ACCELERATION
          ANGULARRATE-VAL
          ANGULARRATE
          QUATERNION-VAL
          QUATERNION
))