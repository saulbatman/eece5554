; Auto-generated. Do not edit!


(cl:in-package imu_driver-msg)


;//! \htmlinclude imu.msg.html

(cl:defclass <imu> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (child_frame_id
    :reader child_frame_id
    :initarg :child_frame_id
    :type cl:string
    :initform "")
   (YPR
    :reader YPR
    :initarg :YPR
    :type geometry_msgs-msg:Vector3
    :initform (cl:make-instance 'geometry_msgs-msg:Vector3))
   (Magnetic
    :reader Magnetic
    :initarg :Magnetic
    :type geometry_msgs-msg:Vector3
    :initform (cl:make-instance 'geometry_msgs-msg:Vector3))
   (Acceleration
    :reader Acceleration
    :initarg :Acceleration
    :type geometry_msgs-msg:Vector3
    :initform (cl:make-instance 'geometry_msgs-msg:Vector3))
   (AngularRate
    :reader AngularRate
    :initarg :AngularRate
    :type geometry_msgs-msg:Vector3
    :initform (cl:make-instance 'geometry_msgs-msg:Vector3))
   (Quaternion
    :reader Quaternion
    :initarg :Quaternion
    :type geometry_msgs-msg:Quaternion
    :initform (cl:make-instance 'geometry_msgs-msg:Quaternion)))
)

(cl:defclass imu (<imu>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <imu>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'imu)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name imu_driver-msg:<imu> is deprecated: use imu_driver-msg:imu instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <imu>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader imu_driver-msg:header-val is deprecated.  Use imu_driver-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'child_frame_id-val :lambda-list '(m))
(cl:defmethod child_frame_id-val ((m <imu>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader imu_driver-msg:child_frame_id-val is deprecated.  Use imu_driver-msg:child_frame_id instead.")
  (child_frame_id m))

(cl:ensure-generic-function 'YPR-val :lambda-list '(m))
(cl:defmethod YPR-val ((m <imu>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader imu_driver-msg:YPR-val is deprecated.  Use imu_driver-msg:YPR instead.")
  (YPR m))

(cl:ensure-generic-function 'Magnetic-val :lambda-list '(m))
(cl:defmethod Magnetic-val ((m <imu>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader imu_driver-msg:Magnetic-val is deprecated.  Use imu_driver-msg:Magnetic instead.")
  (Magnetic m))

(cl:ensure-generic-function 'Acceleration-val :lambda-list '(m))
(cl:defmethod Acceleration-val ((m <imu>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader imu_driver-msg:Acceleration-val is deprecated.  Use imu_driver-msg:Acceleration instead.")
  (Acceleration m))

(cl:ensure-generic-function 'AngularRate-val :lambda-list '(m))
(cl:defmethod AngularRate-val ((m <imu>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader imu_driver-msg:AngularRate-val is deprecated.  Use imu_driver-msg:AngularRate instead.")
  (AngularRate m))

(cl:ensure-generic-function 'Quaternion-val :lambda-list '(m))
(cl:defmethod Quaternion-val ((m <imu>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader imu_driver-msg:Quaternion-val is deprecated.  Use imu_driver-msg:Quaternion instead.")
  (Quaternion m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <imu>) ostream)
  "Serializes a message object of type '<imu>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'child_frame_id))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'child_frame_id))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'YPR) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'Magnetic) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'Acceleration) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'AngularRate) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'Quaternion) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <imu>) istream)
  "Deserializes a message object of type '<imu>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'child_frame_id) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'child_frame_id) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'YPR) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'Magnetic) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'Acceleration) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'AngularRate) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'Quaternion) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<imu>)))
  "Returns string type for a message object of type '<imu>"
  "imu_driver/imu")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'imu)))
  "Returns string type for a message object of type 'imu"
  "imu_driver/imu")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<imu>)))
  "Returns md5sum for a message object of type '<imu>"
  "f4576a997189872207f0c10570329d0e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'imu)))
  "Returns md5sum for a message object of type 'imu"
  "f4576a997189872207f0c10570329d0e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<imu>)))
  "Returns full string definition for message of type '<imu>"
  (cl:format cl:nil "Header header~%string child_frame_id~%geometry_msgs/Vector3 YPR~%geometry_msgs/Vector3 Magnetic~%geometry_msgs/Vector3 Acceleration~%geometry_msgs/Vector3 AngularRate~%~%geometry_msgs/Quaternion Quaternion~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'imu)))
  "Returns full string definition for message of type 'imu"
  (cl:format cl:nil "Header header~%string child_frame_id~%geometry_msgs/Vector3 YPR~%geometry_msgs/Vector3 Magnetic~%geometry_msgs/Vector3 Acceleration~%geometry_msgs/Vector3 AngularRate~%~%geometry_msgs/Quaternion Quaternion~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <imu>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'child_frame_id))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'YPR))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'Magnetic))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'Acceleration))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'AngularRate))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'Quaternion))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <imu>))
  "Converts a ROS message object to a list"
  (cl:list 'imu
    (cl:cons ':header (header msg))
    (cl:cons ':child_frame_id (child_frame_id msg))
    (cl:cons ':YPR (YPR msg))
    (cl:cons ':Magnetic (Magnetic msg))
    (cl:cons ':Acceleration (Acceleration msg))
    (cl:cons ':AngularRate (AngularRate msg))
    (cl:cons ':Quaternion (Quaternion msg))
))
