;; Auto-generated. Do not edit!


(when (boundp 'imu_driver::imu)
  (if (not (find-package "IMU_DRIVER"))
    (make-package "IMU_DRIVER"))
  (shadow 'imu (find-package "IMU_DRIVER")))
(unless (find-package "IMU_DRIVER::IMU")
  (make-package "IMU_DRIVER::IMU"))

(in-package "ROS")
;;//! \htmlinclude imu.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass imu_driver::imu
  :super ros::object
  :slots (_header _child_frame_id _YPR _Magnetic _Acceleration _AngularRate _Quaternion ))

(defmethod imu_driver::imu
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:child_frame_id __child_frame_id) "")
    ((:YPR __YPR) (instance geometry_msgs::Vector3 :init))
    ((:Magnetic __Magnetic) (instance geometry_msgs::Vector3 :init))
    ((:Acceleration __Acceleration) (instance geometry_msgs::Vector3 :init))
    ((:AngularRate __AngularRate) (instance geometry_msgs::Vector3 :init))
    ((:Quaternion __Quaternion) (instance geometry_msgs::Quaternion :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _child_frame_id (string __child_frame_id))
   (setq _YPR __YPR)
   (setq _Magnetic __Magnetic)
   (setq _Acceleration __Acceleration)
   (setq _AngularRate __AngularRate)
   (setq _Quaternion __Quaternion)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:child_frame_id
   (&optional __child_frame_id)
   (if __child_frame_id (setq _child_frame_id __child_frame_id)) _child_frame_id)
  (:YPR
   (&rest __YPR)
   (if (keywordp (car __YPR))
       (send* _YPR __YPR)
     (progn
       (if __YPR (setq _YPR (car __YPR)))
       _YPR)))
  (:Magnetic
   (&rest __Magnetic)
   (if (keywordp (car __Magnetic))
       (send* _Magnetic __Magnetic)
     (progn
       (if __Magnetic (setq _Magnetic (car __Magnetic)))
       _Magnetic)))
  (:Acceleration
   (&rest __Acceleration)
   (if (keywordp (car __Acceleration))
       (send* _Acceleration __Acceleration)
     (progn
       (if __Acceleration (setq _Acceleration (car __Acceleration)))
       _Acceleration)))
  (:AngularRate
   (&rest __AngularRate)
   (if (keywordp (car __AngularRate))
       (send* _AngularRate __AngularRate)
     (progn
       (if __AngularRate (setq _AngularRate (car __AngularRate)))
       _AngularRate)))
  (:Quaternion
   (&rest __Quaternion)
   (if (keywordp (car __Quaternion))
       (send* _Quaternion __Quaternion)
     (progn
       (if __Quaternion (setq _Quaternion (car __Quaternion)))
       _Quaternion)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; string _child_frame_id
    4 (length _child_frame_id)
    ;; geometry_msgs/Vector3 _YPR
    (send _YPR :serialization-length)
    ;; geometry_msgs/Vector3 _Magnetic
    (send _Magnetic :serialization-length)
    ;; geometry_msgs/Vector3 _Acceleration
    (send _Acceleration :serialization-length)
    ;; geometry_msgs/Vector3 _AngularRate
    (send _AngularRate :serialization-length)
    ;; geometry_msgs/Quaternion _Quaternion
    (send _Quaternion :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; string _child_frame_id
       (write-long (length _child_frame_id) s) (princ _child_frame_id s)
     ;; geometry_msgs/Vector3 _YPR
       (send _YPR :serialize s)
     ;; geometry_msgs/Vector3 _Magnetic
       (send _Magnetic :serialize s)
     ;; geometry_msgs/Vector3 _Acceleration
       (send _Acceleration :serialize s)
     ;; geometry_msgs/Vector3 _AngularRate
       (send _AngularRate :serialize s)
     ;; geometry_msgs/Quaternion _Quaternion
       (send _Quaternion :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; string _child_frame_id
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _child_frame_id (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; geometry_msgs/Vector3 _YPR
     (send _YPR :deserialize buf ptr-) (incf ptr- (send _YPR :serialization-length))
   ;; geometry_msgs/Vector3 _Magnetic
     (send _Magnetic :deserialize buf ptr-) (incf ptr- (send _Magnetic :serialization-length))
   ;; geometry_msgs/Vector3 _Acceleration
     (send _Acceleration :deserialize buf ptr-) (incf ptr- (send _Acceleration :serialization-length))
   ;; geometry_msgs/Vector3 _AngularRate
     (send _AngularRate :deserialize buf ptr-) (incf ptr- (send _AngularRate :serialization-length))
   ;; geometry_msgs/Quaternion _Quaternion
     (send _Quaternion :deserialize buf ptr-) (incf ptr- (send _Quaternion :serialization-length))
   ;;
   self)
  )

(setf (get imu_driver::imu :md5sum-) "f4576a997189872207f0c10570329d0e")
(setf (get imu_driver::imu :datatype-) "imu_driver/imu")
(setf (get imu_driver::imu :definition-)
      "Header header
string child_frame_id
geometry_msgs/Vector3 YPR
geometry_msgs/Vector3 Magnetic
geometry_msgs/Vector3 Acceleration
geometry_msgs/Vector3 AngularRate

geometry_msgs/Quaternion Quaternion
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

")



(provide :imu_driver/imu "f4576a997189872207f0c10570329d0e")


