// Auto-generated. Do not edit!

// (in-package imu_driver.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let geometry_msgs = _finder('geometry_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class imu {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.child_frame_id = null;
      this.YPR = null;
      this.Magnetic = null;
      this.Acceleration = null;
      this.AngularRate = null;
      this.Quaternion = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('child_frame_id')) {
        this.child_frame_id = initObj.child_frame_id
      }
      else {
        this.child_frame_id = '';
      }
      if (initObj.hasOwnProperty('YPR')) {
        this.YPR = initObj.YPR
      }
      else {
        this.YPR = new geometry_msgs.msg.Vector3();
      }
      if (initObj.hasOwnProperty('Magnetic')) {
        this.Magnetic = initObj.Magnetic
      }
      else {
        this.Magnetic = new geometry_msgs.msg.Vector3();
      }
      if (initObj.hasOwnProperty('Acceleration')) {
        this.Acceleration = initObj.Acceleration
      }
      else {
        this.Acceleration = new geometry_msgs.msg.Vector3();
      }
      if (initObj.hasOwnProperty('AngularRate')) {
        this.AngularRate = initObj.AngularRate
      }
      else {
        this.AngularRate = new geometry_msgs.msg.Vector3();
      }
      if (initObj.hasOwnProperty('Quaternion')) {
        this.Quaternion = initObj.Quaternion
      }
      else {
        this.Quaternion = new geometry_msgs.msg.Quaternion();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type imu
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [child_frame_id]
    bufferOffset = _serializer.string(obj.child_frame_id, buffer, bufferOffset);
    // Serialize message field [YPR]
    bufferOffset = geometry_msgs.msg.Vector3.serialize(obj.YPR, buffer, bufferOffset);
    // Serialize message field [Magnetic]
    bufferOffset = geometry_msgs.msg.Vector3.serialize(obj.Magnetic, buffer, bufferOffset);
    // Serialize message field [Acceleration]
    bufferOffset = geometry_msgs.msg.Vector3.serialize(obj.Acceleration, buffer, bufferOffset);
    // Serialize message field [AngularRate]
    bufferOffset = geometry_msgs.msg.Vector3.serialize(obj.AngularRate, buffer, bufferOffset);
    // Serialize message field [Quaternion]
    bufferOffset = geometry_msgs.msg.Quaternion.serialize(obj.Quaternion, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type imu
    let len;
    let data = new imu(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [child_frame_id]
    data.child_frame_id = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [YPR]
    data.YPR = geometry_msgs.msg.Vector3.deserialize(buffer, bufferOffset);
    // Deserialize message field [Magnetic]
    data.Magnetic = geometry_msgs.msg.Vector3.deserialize(buffer, bufferOffset);
    // Deserialize message field [Acceleration]
    data.Acceleration = geometry_msgs.msg.Vector3.deserialize(buffer, bufferOffset);
    // Deserialize message field [AngularRate]
    data.AngularRate = geometry_msgs.msg.Vector3.deserialize(buffer, bufferOffset);
    // Deserialize message field [Quaternion]
    data.Quaternion = geometry_msgs.msg.Quaternion.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += _getByteLength(object.child_frame_id);
    return length + 132;
  }

  static datatype() {
    // Returns string type for a message object
    return 'imu_driver/imu';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f4576a997189872207f0c10570329d0e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    string child_frame_id
    geometry_msgs/Vector3 YPR
    geometry_msgs/Vector3 Magnetic
    geometry_msgs/Vector3 Acceleration
    geometry_msgs/Vector3 AngularRate
    
    geometry_msgs/Quaternion Quaternion
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new imu(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.child_frame_id !== undefined) {
      resolved.child_frame_id = msg.child_frame_id;
    }
    else {
      resolved.child_frame_id = ''
    }

    if (msg.YPR !== undefined) {
      resolved.YPR = geometry_msgs.msg.Vector3.Resolve(msg.YPR)
    }
    else {
      resolved.YPR = new geometry_msgs.msg.Vector3()
    }

    if (msg.Magnetic !== undefined) {
      resolved.Magnetic = geometry_msgs.msg.Vector3.Resolve(msg.Magnetic)
    }
    else {
      resolved.Magnetic = new geometry_msgs.msg.Vector3()
    }

    if (msg.Acceleration !== undefined) {
      resolved.Acceleration = geometry_msgs.msg.Vector3.Resolve(msg.Acceleration)
    }
    else {
      resolved.Acceleration = new geometry_msgs.msg.Vector3()
    }

    if (msg.AngularRate !== undefined) {
      resolved.AngularRate = geometry_msgs.msg.Vector3.Resolve(msg.AngularRate)
    }
    else {
      resolved.AngularRate = new geometry_msgs.msg.Vector3()
    }

    if (msg.Quaternion !== undefined) {
      resolved.Quaternion = geometry_msgs.msg.Quaternion.Resolve(msg.Quaternion)
    }
    else {
      resolved.Quaternion = new geometry_msgs.msg.Quaternion()
    }

    return resolved;
    }
};

module.exports = imu;
