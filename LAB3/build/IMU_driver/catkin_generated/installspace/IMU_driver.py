# !/usr/bin/env python

from re import L
import rospy
from std_msgs.msg import String   #message type
from IMU_driver.msg import IMU
import serial
from scipy.spatial.transform import Rotation as R

def serialtalker():
    IMU_pub = rospy.Publisher('IMU_data', IMU, queue_size=10)
    IMU_info = IMU()
    IMU_info.header.frame_id = "IMU"
    IMU_info.child_frame_id = "VN100"
    IMU_info.header.seq=0
    # gps_talker is the node name
    rospy.init_node('IMU_talker', anonymous=True)
    rate = rospy.Rate(40) # rate is 1 because gps rate is around 1
    serial_port = rospy.get_param('~port','/dev/pts/4')
    serial_baud = rospy.get_param('~baudrate',115200)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    while not rospy.is_shutdown():
        line  = str(port.readline())
        # rospy.loginfo(line)
        if line != '':
            a = line.split(",")
            if a[0] == 'b\'$VNYMR':
                # print(line)
                if a[2] != "":
                    # Yaw, Pitch, Roll, Magnetic, Acceleration, and Angular Rate Measurements
                    IMU_info.YPR.x = float(a[1])
                    IMU_info.YPR.y = float(a[2])
                    IMU_info.YPR.z = float(a[3])
                    IMU_info.Magnetic.x = float(a[4])
                    IMU_info.Magnetic.y = float(a[5])
                    IMU_info.Magnetic.z = float(a[6])
                    IMU_info.Acceleration.x = float(a[7])
                    IMU_info.Acceleration.y = float(a[8])
                    IMU_info.Acceleration.z = float(a[9])
                    IMU_info.AngularRate.x = float(a[10])
                    IMU_info.AngularRate.y = float(a[11])
                    IMU_info.AngularRate.z = float(a[12])

                    r = R.from_euler('zyx', [IMU_info.YPR.z, IMU_info.YPR.y, IMU_info.YPR.x], degrees=True)
                    quaternion=r.as_quat()
                    IMU_info.Quaternion.x = quaternion[0]
                    IMU_info.Quaternion.y = quaternion[1]
                    IMU_info.Quaternion.z = quaternion[2]
                    IMU_info.Quaternion.w = quaternion[3]
                   
                    IMU_info.header.stamp=rospy.Time.now()     
        rospy.loginfo(IMU_info)
        IMU_pub.publish(IMU_info)
        rate.sleep()
if __name__ == '__main__':
    try:
        # talker()
        serialtalker()
    except rospy.ROSInitException:
        pass