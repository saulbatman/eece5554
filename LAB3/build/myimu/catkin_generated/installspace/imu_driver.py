# !/usr/bin/env python

from re import L
import rospy
from std_msgs.msg import String   #message type
from myimu.msg import imu
import serial
from scipy.spatial.transform import Rotation as R


def serialtalker():
    IMU_pub = rospy.Publisher('IMU_data', imu, queue_size=10)
    IMU_info = imu()
    IMU_info.header.frame_id = "IMU"
    IMU_info.child_frame_id = "VN100"
    IMU_info.header.seq=0
    # gps_talker is the node name
    rospy.init_node('IMU_talker', anonymous=True)
    rate = rospy.Rate(40) # rate is 1 because gps rate is around 1
    serial_port = rospy.get_param('~port','/dev/pts/6')
    serial_baud = rospy.get_param('~baudrate',115200)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    while not rospy.is_shutdown():
        line  = port.readline()
        line=line.decode('utf-8')
        # rospy.loginfo(line)
        if line != '':
            a = line.split(",")
            if a[0] == '$VNYMR':
                # print(line)
                if a[2] != "":
                    # Yaw, Pitch, Roll, Magnetic, Acceleration, and Angular Rate Measurements
                    IMU_info.ypr = (float(a[1]), float(a[2]), float(a[3]))

                    IMU_info.magnetic = (float(a[4]), float(a[5]), float(a[6]))
                    
                    IMU_info.imu.linear_acceleration = (float(a[7]), float(a[8]), float(a[9]))

                    last = (a[12]).split("*")
                    IMU_info.imu.angular_velocity = (float(a[10]), float(a[11]), float(last[0]))

                    r = R.from_euler('zyx', [IMU_info.ypr.z, IMU_info.ypr.y, IMU_info.ypr.x], degrees=True)
                    quaternion=r.as_quat()
                    IMU_info.imu.orientation = (quaternion[0], quaternion[1], quaternion[2], quaternion[3])
                    IMU_info.header.stamp=rospy.Time.now()     
        # rospy.loginfo(IMU_info)
        IMU_pub.publish(IMU_info)
        rate.sleep()
if __name__ == '__main__':
    try:
        # talker()
        serialtalker()
    except rospy.ROSInitException:
        pass